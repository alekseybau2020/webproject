import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WaitingBroadcastComponent } from './waiting-broadcast.component';

describe('WaitingBroadcastComponent', () => {
  let component: WaitingBroadcastComponent;
  let fixture: ComponentFixture<WaitingBroadcastComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WaitingBroadcastComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WaitingBroadcastComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
