import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutsSidebySideComponent } from './layouts-sideby-side.component';

describe('LayoutsSidebySideComponent', () => {
  let component: LayoutsSidebySideComponent;
  let fixture: ComponentFixture<LayoutsSidebySideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayoutsSidebySideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutsSidebySideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
