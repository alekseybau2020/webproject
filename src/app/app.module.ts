import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {Routes, RouterModule} from '@angular/router';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GuestMainComponent } from './guest-main/guest-main.component';
import { WaitingBroadcastComponent } from './waiting-broadcast/waiting-broadcast.component';
import { ViewerMeetingComponent } from './viewer-meeting/viewer-meeting.component';
import { GuestMeetingComponent } from './guest-meeting/guest-meeting.component';
import { MainScreenComponent } from './main-screen/main-screen.component';
import { CreateNewSessionComponent } from './create-new-session/create-new-session.component';
import { MeetingDetailsComponent } from './meeting-details/meeting-details.component';
import { EmptyMeetingAdminComponent } from './empty-meeting-admin/empty-meeting-admin.component';
import { EmptyMeetingGuestComponent } from './empty-meeting-guest/empty-meeting-guest.component';
import { SpeakerCamMicTestComponent } from './speaker-cam-mic-test/speaker-cam-mic-test.component';
import { DevicesSettingsComponent } from './devices-settings/devices-settings.component';
import { ErrorComponent } from './error/error.component';
import { WaitingBroadcastViewerComponent } from './waiting-broadcast-viewer/waiting-broadcast-viewer.component';
import { ViewerSeengMCUComponent } from './viewer-seeng-mcu/viewer-seeng-mcu.component';
import { MeetingRaisendHandComponent } from './meeting-raisend-hand/meeting-raisend-hand.component';
import { WebCastHostCamiMicTestComponent } from './web-cast-host-cami-mic-test/web-cast-host-cami-mic-test.component';
import { MeetingComponent } from './meeting/meeting.component';
import { MeetingLiveComponent } from './meeting-live/meeting-live.component';
import { BroadcastSettingsComponent } from './broadcast-settings/broadcast-settings.component';
import { LayoutsOneComponent } from './layouts-one/layouts-one.component';
import { LayoutsSidebySideComponent } from './layouts-sideby-side/layouts-sideby-side.component';
import { LayoutsMosaicComponent } from './layouts-mosaic/layouts-mosaic.component';
import { LayoutsSidebarComponent } from './layouts-sidebar/layouts-sidebar.component';
import { LayoutsScreenshareComponent } from './layouts-screenshare/layouts-screenshare.component';
import { TestComponent } from './test/test.component';

 
// определение маршрутов
const appRoutes: Routes =[            
   
  
];

@NgModule({
  declarations: [
    AppComponent,   
    GuestMainComponent,   
    WaitingBroadcastComponent,   
    ViewerMeetingComponent,
    GuestMeetingComponent,
    MainScreenComponent,
    CreateNewSessionComponent,
    MeetingDetailsComponent,
    EmptyMeetingAdminComponent,
    EmptyMeetingGuestComponent,
    SpeakerCamMicTestComponent,
    DevicesSettingsComponent,
    ErrorComponent,
    WaitingBroadcastViewerComponent,
    ViewerSeengMCUComponent,
    MeetingRaisendHandComponent,
    WebCastHostCamiMicTestComponent,
    MeetingComponent,
    MeetingLiveComponent,
    BroadcastSettingsComponent,
    LayoutsOneComponent,
    LayoutsSidebySideComponent,
    LayoutsMosaicComponent,
    LayoutsSidebarComponent,
    LayoutsScreenshareComponent,
    TestComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
