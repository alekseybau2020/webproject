import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MainScreenComponent} from './main-screen/main-screen.component';
import {CreateNewSessionComponent} from './create-new-session/create-new-session.component';
import {EmptyMeetingAdminComponent} from './empty-meeting-admin/empty-meeting-admin.component';
import {EmptyMeetingGuestComponent} from './empty-meeting-guest/empty-meeting-guest.component';
import {SpeakerCamMicTestComponent} from './speaker-cam-mic-test/speaker-cam-mic-test.component';
import {DevicesSettingsComponent} from './devices-settings/devices-settings.component';
import {ErrorComponent} from './error/error.component';
import {WaitingBroadcastViewerComponent} from './waiting-broadcast-viewer/waiting-broadcast-viewer.component';
import {ViewerSeengMCUComponent} from './viewer-seeng-mcu/viewer-seeng-mcu.component';
import {ViewerMeetingComponent} from './viewer-meeting/viewer-meeting.component';
import {GuestMeetingComponent} from './guest-meeting/guest-meeting.component';
import {MeetingRaisendHandComponent} from './meeting-raisend-hand/meeting-raisend-hand.component';
import {WebCastHostCamiMicTestComponent} from './web-cast-host-cami-mic-test/web-cast-host-cami-mic-test.component';
import {MeetingComponent} from './meeting/meeting.component';
import {MeetingLiveComponent} from './meeting-live/meeting-live.component';
import {BroadcastSettingsComponent} from './broadcast-settings/broadcast-settings.component';
import { LayoutsOneComponent } from './layouts-one/layouts-one.component';
import { LayoutsSidebySideComponent } from './layouts-sideby-side/layouts-sideby-side.component';
import { LayoutsMosaicComponent } from './layouts-mosaic/layouts-mosaic.component';
import { LayoutsSidebarComponent } from './layouts-sidebar/layouts-sidebar.component';
import { LayoutsScreenshareComponent } from './layouts-screenshare/layouts-screenshare.component';
import { TestComponent } from './test/test.component';

const routes: Routes = [
  { path: '1', component: MainScreenComponent},
  { path: '2', component: CreateNewSessionComponent},
  { path: '3', component: EmptyMeetingAdminComponent},
  { path: '4', component: EmptyMeetingGuestComponent},
  { path: '5', component: SpeakerCamMicTestComponent},
  { path: '6', component: DevicesSettingsComponent},
  { path: '7', component: ErrorComponent},
  { path: '8', component: WaitingBroadcastViewerComponent},
  { path: '9', component: ViewerSeengMCUComponent},
  { path: '10', component: ViewerMeetingComponent},
  { path: '11', component: GuestMeetingComponent},
  { path: '12', component: MeetingRaisendHandComponent},
  { path: '13', component: WebCastHostCamiMicTestComponent},
  { path: '14', component: MeetingComponent},
  { path: '15', component: MeetingLiveComponent},
  { path: '16', component: BroadcastSettingsComponent},
  { path: '17', component: LayoutsOneComponent},
  { path: '18', component: LayoutsSidebySideComponent},
  { path: '19', component: LayoutsMosaicComponent},
  { path: '20', component: LayoutsSidebarComponent},
  { path: '21', component: LayoutsScreenshareComponent},
  { path: '22', component: TestComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
  