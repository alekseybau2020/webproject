import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WaitingBroadcastViewerComponent } from './waiting-broadcast-viewer.component';

describe('WaitingBroadcastViewerComponent', () => {
  let component: WaitingBroadcastViewerComponent;
  let fixture: ComponentFixture<WaitingBroadcastViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WaitingBroadcastViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WaitingBroadcastViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
