import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewerMeetingComponent } from './viewer-meeting.component';

describe('ViewerMeetingComponent', () => {
  let component: ViewerMeetingComponent;
  let fixture: ComponentFixture<ViewerMeetingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewerMeetingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewerMeetingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
