import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutsOneComponent } from './layouts-one.component';

describe('LayoutsOneComponent', () => {
  let component: LayoutsOneComponent;
  let fixture: ComponentFixture<LayoutsOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayoutsOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutsOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
