import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeetingLiveComponent } from './meeting-live.component';

describe('MeetingLiveComponent', () => {
  let component: MeetingLiveComponent;
  let fixture: ComponentFixture<MeetingLiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeetingLiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeetingLiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
