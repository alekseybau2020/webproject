import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpeakerCamMicTestComponent } from './speaker-cam-mic-test.component';

describe('SpeakerCamMicTestComponent', () => {
  let component: SpeakerCamMicTestComponent;
  let fixture: ComponentFixture<SpeakerCamMicTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpeakerCamMicTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpeakerCamMicTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
