import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeetingRaisendHandComponent } from './meeting-raisend-hand.component';

describe('MeetingRaisendHandComponent', () => {
  let component: MeetingRaisendHandComponent;
  let fixture: ComponentFixture<MeetingRaisendHandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeetingRaisendHandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeetingRaisendHandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
