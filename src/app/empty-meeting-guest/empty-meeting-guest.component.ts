import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-empty-meeting-guest',
  templateUrl: './empty-meeting-guest.component.html',
  styleUrls: ['./empty-meeting-guest.component.scss']
})
export class EmptyMeetingGuestComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
