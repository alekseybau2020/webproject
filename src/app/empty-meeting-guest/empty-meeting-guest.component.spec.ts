import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmptyMeetingGuestComponent } from './empty-meeting-guest.component';

describe('EmptyMeetingGuestComponent', () => {
  let component: EmptyMeetingGuestComponent;
  let fixture: ComponentFixture<EmptyMeetingGuestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmptyMeetingGuestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmptyMeetingGuestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
