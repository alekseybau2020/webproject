import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmptyMeetingAdminComponent } from './empty-meeting-admin.component';

describe('EmptyMeetingAdminComponent', () => {
  let component: EmptyMeetingAdminComponent;
  let fixture: ComponentFixture<EmptyMeetingAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmptyMeetingAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmptyMeetingAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
