import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewerSeengMCUComponent } from './viewer-seeng-mcu.component';

describe('ViewerSeengMCUComponent', () => {
  let component: ViewerSeengMCUComponent;
  let fixture: ComponentFixture<ViewerSeengMCUComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewerSeengMCUComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewerSeengMCUComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
