import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebCastHostCamiMicTestComponent } from './web-cast-host-cami-mic-test.component';

describe('WebCastHostCamiMicTestComponent', () => {
  let component: WebCastHostCamiMicTestComponent;
  let fixture: ComponentFixture<WebCastHostCamiMicTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebCastHostCamiMicTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebCastHostCamiMicTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
