import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutsSidebarComponent } from './layouts-sidebar.component';

describe('LayoutsSidebarComponent', () => {
  let component: LayoutsSidebarComponent;
  let fixture: ComponentFixture<LayoutsSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayoutsSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutsSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
