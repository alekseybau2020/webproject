import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestMeetingComponent } from './guest-meeting.component';

describe('GuestMeetingComponent', () => {
  let component: GuestMeetingComponent;
  let fixture: ComponentFixture<GuestMeetingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuestMeetingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestMeetingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
