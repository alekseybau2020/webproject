import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutsMosaicComponent } from './layouts-mosaic.component';

describe('LayoutsMosaicComponent', () => {
  let component: LayoutsMosaicComponent;
  let fixture: ComponentFixture<LayoutsMosaicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayoutsMosaicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutsMosaicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
