import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BroadcastSettingsComponent } from './broadcast-settings.component';

describe('BroadcastSettingsComponent', () => {
  let component: BroadcastSettingsComponent;
  let fixture: ComponentFixture<BroadcastSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BroadcastSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BroadcastSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
