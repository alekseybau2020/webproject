import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutsScreenshareComponent } from './layouts-screenshare.component';

describe('LayoutsScreenshareComponent', () => {
  let component: LayoutsScreenshareComponent;
  let fixture: ComponentFixture<LayoutsScreenshareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayoutsScreenshareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutsScreenshareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
